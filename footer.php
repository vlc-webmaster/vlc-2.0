<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Heisenberg
 */
?>

			</div><!-- #content -->

			<footer id="colophon" class="site-footer" role="contentinfo">

				<div class="column row"><!-- .column.row start -->

					<p class="text-center">This is the footer</p>

				</div><!-- .column.row end -->

			</footer><!-- #colophon -->

		</div> <!-- .off-canvas-content -->
	</div><!-- .off-canvas-wrapper-inner -->
</div><!-- .off-canvas-wrapper -->

<?php wp_footer(); ?>
</body>
</html>
